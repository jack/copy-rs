use clipboard::{ClipboardContext, ClipboardProvider};
use std::{io, io::Read};

fn main() {
    // Create a buffer to hold the input from stdin
    let mut buffer = String::new();

    // Open stdin and read it into the buffer
    io::stdin()
        .lock()
        .read_to_string(&mut buffer)
        .expect("error reading stdin into buffer");

    // Create clipboard context
    let mut ctx: ClipboardContext =
        ClipboardProvider::new().expect("cannot open clipboard context");

    // Set the contents of the clipboard
    ctx.set_contents(buffer)
        .expect("cannot set clipboard contents");

    #[cfg(target_os = "linux")]
    {
        // If we're on linux, the process has to stay running to serve the
        // clipboard contents when requested
        wait();
    }
}

#[cfg(target_os = "linux")]
fn wait() {
    use std::{thread, time};

    // Set how long we'll wait (1 minute)
    let timeout = time::Duration::from_secs(60);

    // Wait for the user to paste the contents.
    // This is a hack to make this work on Linux, where the program providing
    // the copied content must be up when the paste requests it.
    eprintln!(
        "You now have {} seconds to paste before this process exits...",
        timeout.as_secs()
    );

    // Wait for our timeout to elapse
    thread::sleep(timeout);
}
