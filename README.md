# copy-rs

Cross-platform (GNU/Linux and MacOS) CLI copy command.

# Usage

To use, simply pipe any content you want into the `copy` command. Example:

``` sh
cat /etc/resolv.conf | copy
```

Due to limitations of the way the clipboard works on GNU/Linux, the `copy` process
will block for 60 seconds while you paste the contents somewhere. After that,
the copied material will be unavailable for pasting. On MacOS, it does not block
and there's no time limit.

# Installation

Currently, only installation from source is supported.

1. Clone this repo
2. Install X dev package packages:
  * Ubuntu:
    ```
    $ sudo apt-get install \
      xorg-dev \
      libxcb-render0-dev \
      libxcb-shape0-dev \
      libxcb-xfixes0-dev
    ```
3. Run `cargo build --release`
4. Copy the resulting binary to wherever you prefer: `sudo cp
   target/release/copy /usr/local/bin`

